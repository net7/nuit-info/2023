/* eslint-disable @typescript-eslint/triple-slash-reference */
/// <reference types="@sveltejs/kit" />
/// <reference no-default-lib="true"/>
/// <reference lib="esnext" />
/// <reference lib="webworker" />
import * as $serviceWorker from '$service-worker';
const sw = self as unknown as ServiceWorkerGlobalScope;

// Create a unique cache name for this deployment
const CACHE = `cache-${$serviceWorker.version}`;

const ASSETS = [
	...$serviceWorker.build, // the app itself
	...$serviceWorker.files // everything in `static`
];

sw.addEventListener('install', async (event) => {
	// Create a new cache and add all files to it
	const addFilesToCache = async () => {
		const cache = await caches.open(CACHE);
		await cache.addAll(ASSETS);
	};

	event.waitUntil(addFilesToCache());
});

sw.addEventListener('activate', async (event) => {
	// Remove previous cached data from disk
	const deleteOldCaches = async () => {
		for (const key of await caches.keys()) if (key !== CACHE) await caches.delete(key);
	};

	event.waitUntil(deleteOldCaches());
});

sw.addEventListener('fetch', (event) => {
	// ignore POST requests etc
	if (event.request.method !== 'GET') return;

	const respond = async () => {
		const url = new URL(event.request.url);
		const cache = await caches.open(CACHE);

		// `build`/`files` can always be served from the cache
		if (ASSETS.includes(url.pathname)) {
			const response = await cache.match(url.pathname);
			if (response === undefined) throw new Error('Cache miss in build or files');
			return response;
		}

		// for everything else, try the network first, but
		// fall back to the cache if we're offline
		try {
			const response = await fetch(event.request);

			if (response.status === 200) await cache.put(event.request, response.clone());

			return response;
		} catch {
			const response = await cache.match(event.request);
			if (response === undefined) throw new Error('Cache miss');
			return response;
		}
	};

	event.respondWith(respond());
});
