import { lucia } from "lucia";
import { prisma as prismaAdapter } from '@lucia-auth/adapter-prisma';
import { sveltekit } from 'lucia/middleware';
import prisma from './prisma';

export const auth = lucia({
	env: process.env.NODE_ENV === 'development' ? 'DEV' : 'PROD',
	adapter: prismaAdapter(prisma, {
		user: "user",
		key: "key",
		session: "session",
	}),
	middleware: sveltekit(),

	getUserAttributes: (userData) => {
		return {
			id: userData.id,
			name: userData.name,
			email: userData.email,
			admin: userData.admin,
			isAsso: userData.isAsso,
			bio: userData.bio,
			avatar: userData.avatar,
			website: userData.website
		};
	}
});

export type Auth = typeof auth;