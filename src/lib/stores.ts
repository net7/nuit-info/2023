import { browser } from '$app/environment';
import { writable } from 'svelte/store';
export let theme = writable('light');

export let desastre = writable(false);
if (browser) {
	theme = writable(localStorage.getItem('theme') || 'light');
	theme.subscribe((val) => localStorage.setItem('theme', val));

	desastre = writable(localStorage.getItem('desastre') === 'true' || false);
	desastre.subscribe((val) => localStorage.setItem('desastre', val.toString()));
}
