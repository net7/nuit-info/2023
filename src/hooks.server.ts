import { auth } from '$lib/server/lucia';
import { redirect, type Handle } from '@sveltejs/kit';
import { sequence } from "@sveltejs/kit/hooks"


export const isAuthenticated: Handle = async ({ resolve, event }) => {
    if (event.url.pathname.startsWith('/login')) {
        return resolve(event);
    }
    const locals = event.locals;
	const session = await locals.auth.validate();
	if (!session) {
        throw redirect(302, '/login')
    }

    return resolve(event);
}

export const lucia: Handle = async ({ event, resolve }) => {
	// we can pass `event` because we used the SvelteKit middleware
	event.locals.auth = auth.handleRequest(event);
	return await resolve(event);
};

export const handle = sequence(lucia);