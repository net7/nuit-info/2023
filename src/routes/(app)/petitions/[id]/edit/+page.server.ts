import type {Actions, PageServerLoad} from '.././$types';
import {unified} from "unified";
import rehypeParse from 'rehype-parse';
import rehypeRemark from 'rehype-remark';
import remarkStringify from 'remark-stringify';
import {error, redirect} from "@sveltejs/kit";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeSanitize from "rehype-sanitize";
import rehypeStringify from "rehype-stringify";

export const load: PageServerLoad = async ({params, locals}) => {
    const session = await locals.auth.validate();
    if (!session?.user.admin && !session?.user.isAsso) throw error(401, 'Unauthorized');

    const petition = await prisma.petition.findFirstOrThrow({
        where: {
            id: params.id
        },
        include: {
            creator: true,
            tags: true
        }
    });

    // check if the user is the creator
    if (petition.creatorId !== session.user.id && !session?.user.admin) throw error(401, 'Unauthorized');

    // transform the description from html to markdown
    const description = unified()
        .use(rehypeParse)
        .use(rehypeRemark)
        .use(remarkStringify)
        .processSync(petition.description).toString()

    return {
        petition: {
            ...petition,
            description
        },
        tags: prisma.tag.findMany()
    }
};

export const actions: Actions = {
    default: async ({request, locals}) => {
        const session = await locals.auth.validate();
        if (!session?.user.admin && !session?.user.isAsso) throw error(401, 'Unauthorized');

        const form = await request.formData();
        const id = form.get('id');
        if (!id) throw error(400, 'Invalid form data');

        // get the petition and check if the user is the creator
        let petition = await prisma.petition.findFirstOrThrow({
            where: {
                id: id.toString()
            }
        });
        if (petition.creatorId !== session.user.id) throw error(401, 'Unauthorized');

        // this is a valid request, we can update the petition
        const name = form.get('title');
        const description = form.get('description');
        const startsAt = form.get('startsAt');
        const endsAt = form.get('endsAt');
        const tags = form.getAll('tags');

        // check for empty values
        if (typeof name !== 'string' || typeof description !== 'string' || typeof startsAt !== 'string' || typeof endsAt !== 'string') {
            throw error(400, 'Invalid form data');
        }

        // check dates
        if (new Date(startsAt) > new Date(endsAt)) {
            throw error(400, 'Invalid dates');
        }

        const html = unified()
            .use(remarkParse)
            .use(remarkRehype)
            .use(rehypeSanitize)
            .use(rehypeStringify)
            .processSync(description).toString()

        // update the petition
        petition = await prisma.petition.update({
            where: {
                id: petition.id
            },
            data: {
                name,
                description: html,
                startsAt: new Date(startsAt),
                endsAt: new Date(endsAt),
                tags: {
                    connect: tags.map(tag => ({id: tag.toString()}))
                }
            }
        });


        await prisma.log.create({
            data: {
                user: {
                    connect: {
                        id: session.user.id
                    }
                },
                area: 'petition',
                message: `Petition ${petition.id} updated`
            }
        });

        // Redirect the user to the demonstration page.
        throw redirect(302, `/petitions/${petition.id}`);
    }
}
