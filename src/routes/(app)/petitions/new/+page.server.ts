import type {Actions, PageServerLoad} from './$types';
import {error, redirect} from '@sveltejs/kit';

import rehypeSanitize from 'rehype-sanitize'
import rehypeStringify from 'rehype-stringify'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype'
import {unified} from 'unified'

export const load: PageServerLoad = async ({locals}) => {
    const session = await locals.auth.validate();

    if (!session?.user.admin && !session?.user.isAsso) throw error(401, 'Unauthorized');

    return {tags: prisma.tag.findMany()}
};

export const actions: Actions = {
    default: async ({request, locals}) => {
        const session = await locals.auth.validate();
        if (!session?.user.admin && !session?.user.isAsso) throw error(401, 'Unauthorized');

        const form = await request.formData();
        const name = form.get('title');
        const description = form.get('description');
        const startsAt = form.get('startsAt');
        const endsAt = form.get('endsAt');
        const tags = form.getAll('tags');


        // check for empty values
        if (typeof name !== 'string' || typeof description !== 'string' || typeof startsAt !== 'string' || typeof endsAt !== 'string') {
            throw error(400, 'Invalid form data');
        }

        // check dates
        if (new Date(startsAt) > new Date(endsAt)) {
            throw error(400, 'Invalid dates');
        }

        const html = unified()
            .use(remarkParse)
            .use(remarkRehype)
            .use(rehypeSanitize)
            .use(rehypeStringify)
            .processSync(description).toString()

        // create the petition
        const petition = await prisma.petition.create({
            data: {
                name,
                description: html,
                startsAt: new Date(startsAt),
                endsAt: new Date(endsAt),
                creator: {
                    connect: {
                        id: session.user.id
                    }
                },
                tags: {
                    connect: tags.map(tag => ({id: tag.toString()}))
                }
            }
        });


        await prisma.log.create({
            data: {
                user: {
                    connect: {
                        id: session.user.id
                    }
                },
                area: 'petition',
                message: `Petition ${petition.id} created`
            }
        });

        // Redirect the user to the demonstration page.
        throw redirect(302, `/petitions/${petition.id}`);
    }
}