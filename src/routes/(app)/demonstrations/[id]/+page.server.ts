import type { Actions, PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ params, locals }) => {
	const demonstration = await prisma.demonstration.findFirstOrThrow({
		where: {
			id: params.id
		},
		include: {
			comments: {
				include: {
					user: {
						select: {
							id: true,
							avatar: true,
							name: true
						}
					}
				}
			},
			_count: true
		}
	});

	const session = await locals.auth.validate();
	const user = session?.user;

	return {
		demonstration,
		user
	};
};

export const actions: Actions = {
	async participate({ params, locals }) {
		const session = await locals.auth.validate();

		if (!session) {
			return {
				status: 401,
				body: {
					message: 'Unauthorized'
				}
			};
		}

		const demonstration = await prisma.demonstration.update({
			where: {
				id: params.id
			},
			data: {
				participants: {
					connect: {
						id: session.user.id
					}
				}
			}
		});

        await prisma.log.create({
            data: {
                area: 'petition',
                message: `User ${session.id} participated in demonstration ${demonstration.id}`,
                user: {
                    connect: {
                        id: session.user.id
                    }
                }
            }
        });

		return {
			demonstration
		};
	},

	async comment({ request, params, locals }) {
		const session = await locals.auth.validate();

		if (!session) {
			return {
				status: 401,
				body: {
					message: 'Unauthorized'
				}
			};
		}

		const data = await request.formData();
		const text = data.get('text');

		const comment = await prisma.comment.create({
			data: {
				text: text as string,
				user: {
					connect: {
						id: session.user.id
					}
				},
				demonstration: {
					connect: {
						id: params.id
					}
				}
			}
		});

        await prisma.log.create({
            data: {
                area: 'petition',
                message: `User ${session.id} commented on demonstration ${comment.id}`,
                user: {
                    connect: {
                        id: session.user.id
                    }
                }
            }
        });

		return {
			comment
		};
	}
};