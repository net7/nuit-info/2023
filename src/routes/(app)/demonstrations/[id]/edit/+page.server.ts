import type {Actions, PageServerLoad} from './$types';
import {unified} from "unified";
import rehypeParse from 'rehype-parse';
import rehypeRemark from 'rehype-remark';
import remarkStringify from 'remark-stringify';
import {error, redirect} from "@sveltejs/kit";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeSanitize from "rehype-sanitize";
import rehypeStringify from "rehype-stringify";

export const load: PageServerLoad = async ({params, locals}) => {
    const session = await locals.auth.validate();
    if (!session?.user.admin && !session?.user.isAsso) throw error(401, 'Unauthorized');

    const demonstration = await prisma.demonstration.findFirstOrThrow({
        where: {
            id: params.id
        },
        include: {
            creator: true,
            tags: true
        }
    });

    // check if the user is the creator
    if (demonstration.creatorId !== session.user.id && !session?.user.admin) throw error(401, 'Unauthorized');

    // transform the description from html to markdown
    const description = unified()
        .use(rehypeParse)
        .use(rehypeRemark)
        .use(remarkStringify)
        .processSync(demonstration.description).toString()

    return {
        demonstration: {
            ...demonstration,
            description
        },
        tags: prisma.tag.findMany()
    }
};

export const actions: Actions = {
    default: async ({request, locals}) => {
        const session = await locals.auth.validate();
        if (!session?.user.admin && !session?.user.isAsso) throw error(401, 'Unauthorized');

        const form = await request.formData();
        const id = form.get('id');
        if (!id) throw error(400, 'Invalid form data');

        // get the petition and check if the user is the creator
        let demonstration = await prisma.demonstration.findFirstOrThrow({
            where: {
                id: id.toString()
            }
        });
        if (demonstration.creatorId !== session.user.id) throw error(401, 'Unauthorized');

        // this is a valid request, we can update the petition
        const name = form.get('title');
        const description = form.get('description');
        const startsAt = form.get('startsAt');
        const endsAt = form.get('endsAt');
        const tags = form.getAll('tags');

        // check for empty values
        if (typeof name !== 'string' || typeof description !== 'string' || typeof startsAt !== 'string' || typeof endsAt !== 'string') {
            throw error(400, 'Invalid form data');
        }

        // check dates
        if (new Date(startsAt) > new Date(endsAt)) {
            throw error(400, 'Invalid dates');
        }

        const html = unified()
            .use(remarkParse)
            .use(remarkRehype)
            .use(rehypeSanitize)
            .use(rehypeStringify)
            .processSync(description).toString()

        // update the demonstration
        demonstration = await prisma.demonstration.update({
            where: {
                id: demonstration.id
            },
            data: {
                name,
                description: html,
                startsAt: new Date(startsAt),
                endsAt: new Date(endsAt),
                // Connect all tags
                tags: {
                    connect: tags.map(tag => ({
                        id: tag.toString()
                    }))
                },
                creator: {
                    connect: {
                        id: session.user.id
                    }
                }
            }
        });


        await prisma.log.create({
            data: {
                user: {
                    connect: {
                        id: session.user.id
                    }
                },
                area: 'demonstration',
                message: `Demonstration ${demonstration.id} updated`
            }
        });

        // Redirect the user to the demonstration page.
        throw redirect(302, `/demonstration/${demonstration.id}`);
    }
}
