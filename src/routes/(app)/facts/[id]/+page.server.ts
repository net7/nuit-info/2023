import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ params }) => {
	const fact = await prisma.fact.findFirstOrThrow({
		where: {
			id: params.id
		},
		include: {
			comments: {
				include: {
					user: {
						select: {
							avatar: true,
							name: true
						}
					}
				}
			}
		}
	});

	if (fact.deckMap) {
		throw redirect(302, `/maps/${fact.deckMap}.html?from=/facts/`);
	}

	return {
		fact
	};
};
