import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async () => {
	const facts = prisma.fact.findMany({
		include: {
			tags: true
		}
	});

	return {
		facts
	};
};
