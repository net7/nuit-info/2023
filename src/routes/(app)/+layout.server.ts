import type {LayoutServerLoad} from './$types';

export const load: LayoutServerLoad = async ({locals}) => {
    const session = await locals.auth.validate();
    if (session) {
        return {
            admin: session?.user?.admin,
            isAsso: session?.user?.isAsso,
        };
    }
};