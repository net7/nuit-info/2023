import type {PageServerLoad} from './$types';
import {unified} from "unified";
import rehypeParse from 'rehype-parse';
import rehypeRemark from 'rehype-remark';
import {error} from "@sveltejs/kit";

export const load: PageServerLoad = async ({params, locals}) => {
    const session = await locals.auth.validate();
    if (!session?.user.admin && !session?.user.isAsso) throw error(401, 'Unauthorized');
};
