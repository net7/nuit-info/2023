import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async () => {
	const normal = prisma.fact.findMany({
		include: {
			tags: true
		}
	});
    
	return {
		normal
	};
};
