import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ locals }) => {

	const session = await locals.auth.validate();
  if (!session) throw redirect(307, '/login');
  const user = session?.user;
    
  return {
      user
  }
};