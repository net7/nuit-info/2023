import { error, redirect } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';
import {auth} from '$lib/server/lucia';

export const load: PageServerLoad = async ({ locals }) => {

	const session = await locals.auth.validate();
  if (!session) throw error(401, 'Unauthorized')
  const user = session?.user;
    
  return {
      user
  }
};

export const actions: Actions = {
  async profile ({request, locals}) {
      const session = await locals.auth.validate();

      if (!session) {
          return {
              status: 401,
              body: {
                  message: 'Unauthorized'
              }
          }
      }

      const data = await request.formData();
      const name = data.get("name");
      const email = data.get("email");
      const bio = data.get("bio");
      const website = data.get("website");

      // check for empty values
      if (typeof name !== 'string' || typeof email !== 'string' || typeof bio !== 'string' || typeof website !== 'string') {
        throw error(400, 'Invalid form data');
    }

      const user = await prisma.user.update({
          where: {
              id: session.user.id
          },
          data: {
            name, 
            email,
            bio,
            website
          }
      });

      await prisma.log.create({
          data: {
              area: 'user',
              message: `User ${name} changed profile.`,
              user: {
                  connect: {
                      id: session.user.id
                  }
              }
          }
      });

      throw redirect(302, "/profile");
  },


  async password ({request, locals}) {
    const session = await locals.auth.validate();

    if (!session) {
        return {
            status: 401,
            body: {
                message: 'Unauthorized'
            }
        }
    }

    const data = await request.formData();
    const passwordCurrent = data.get("passwordCurrent");
    const passwordNew = data.get("passwordNew");
    const passwordConf = data.get("passwordConf");

    // check for empty values
    if (typeof passwordCurrent !== 'string' || typeof passwordNew !== 'string' || typeof passwordConf !== 'string') {
      throw error(400, 'Invalid form data');
  }

    if (passwordNew !== passwordConf) {
      throw error(400, 'Passwords do not match');
    }

    const user = session.user;

    // Update password
    await auth.updateKeyPassword("local", user.email, passwordNew);

    throw redirect(302, "/profile");
}
  
};