import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ locals, params }) => {

	const session = await locals.auth.validate();
  if (!session) throw error(401, 'You must be connected')
    
  return {
      user: prisma.user.findUniqueOrThrow({
        where: {
          id: params.id
        }
      })
  }
};