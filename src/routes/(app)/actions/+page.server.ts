import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async () => {
	const demonstrations = await prisma.demonstration.findMany({
		include: {
			creator: true,
			tags: true,
			_count: true
		},
		orderBy: {
			startsAt: 'asc'
		},
		where: {
			endsAt: {
				gt: new Date()
			}
		}
	});
	const petitions = await prisma.petition.findMany({
		include: {
			creator: true,
			tags: true,
			_count: true
		},
		orderBy: {
			startsAt: 'asc'
		}
	});

	// join demonstrations and petitions and sort by startsAt asc
	const actions = [
		...demonstrations.map((d) => ({ ...d, type: 'Demonstration' })),
		...petitions.map((p) => ({ ...p, type: 'Petition' }))
	] as Array<
		| ({ type: 'Demonstration' } & (typeof demonstrations)[number])
		| ({ type: 'Petition' } & (typeof petitions)[number])
	>;

	actions.sort((a, b) => a.startsAt.getTime() - b.startsAt.getTime());

	return {
		actions
	};
};
