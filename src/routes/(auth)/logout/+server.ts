import  {error} from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import { auth } from '$lib/server/lucia';
import { redirect } from '@sveltejs/kit';

export const GET: RequestHandler =  async ({ locals }) => {
		const session = await locals.auth.validate();
		if (!session) throw error(401, 'Unauthorized');
		await auth.invalidateSession(session.sessionId); // invalidate session
		locals.auth.setSession(null); // remove cookie
		// redirect to home page
		throw redirect(302, "/"); // redirect to login page
};
