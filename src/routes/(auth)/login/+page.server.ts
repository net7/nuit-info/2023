import {auth} from '$lib/server/lucia';
import type {Actions} from './$types';
import {error, redirect} from '@sveltejs/kit';
import type {PageServerLoad} from "./$types";

export const load: PageServerLoad = async ({ locals}) => {
    const session = await locals.auth.validate();
    if (session?.user) throw redirect(307, '/');
};

export const actions: Actions = {
    default: async ({request, locals}) => {
        const form = await request.formData();
        const email = form.get('email');
        const password = form.get('password');
        // check for empty values
        if (typeof email !== 'string' || typeof password !== 'string') {
            throw error(400, 'Invalid form data');
        }
        const lowerEmail = email.toLowerCase();

        // Get the user key.
        const key = await auth.useKey('local', lowerEmail, password);
        // Create a session for the user.
        const user = await auth.getUser(key.userId);

        await prisma.log.create({
            data: {
                user: {
                    connect: {
                        id: user.id
                    }
                },
                area: 'login',
                message: 'User registered'
            }
        });

        await auth.deleteDeadUserSessions(key.userId);

        const session = await auth.createSession({
            userId: key.userId,
            attributes: {}
        });
        // Redirect the user to the login page.
        locals.auth.setSession(session);
        // Redirect the user to the home page.
        throw redirect(303, '/');
    }
}
