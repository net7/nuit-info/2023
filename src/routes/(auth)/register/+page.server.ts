import { auth } from '$lib/server/lucia';
import type { Actions } from './$types';
import {error, redirect} from '@sveltejs/kit';
import { LuciaError } from 'lucia';
import type {PageServerLoad} from "./$types";

export const load: PageServerLoad = async ({ locals}) => {
    const session = await locals.auth.validate();
    if (session?.user) throw redirect(307, '/');
};

export const actions: Actions = {
	default: async ({ request, locals }) => {
		const form = await request.formData();
		const username = form.get('username');
        const email = form.get('email');
		const password = form.get('password');
        const passwordconfirm = form.get('password-confirm');
		// check for empty values
		if (typeof username !== 'string' || typeof password !== 'string' || typeof email !== 'string' || typeof passwordconfirm !== 'string') {
            throw error(400, 'Invalid form data');
        }
        if (password !== passwordconfirm) {
            throw error(400, 'Passwords do not match');
        }

        try {
            const user = await auth.createUser({
                key: {
                    providerId: "local",
                    providerUserId: email.toLowerCase(),
                    password
                },
                attributes: {
                    name: username,
                    email: email.toLowerCase(),
                    isAsso: false,
                    admin: false,
                    bio: '',
                    avatar: '',
                }, // expects `Lucia.DatabaseUserAttributes`
            });

            await prisma.log.create({
                data: {
                    user: {
                        connect: {
                            id: user.id
                        }
                    },
                    area: 'register',
                    message: 'User registered'
                }
            });

        // Get the user key.
		const key = await auth.useKey('local', email.toLocaleLowerCase(), password);
		// Create a session for the user.
		const session = await auth.createSession({
			userId: key.userId,
			attributes: {}
		});
		// Redirect the user to the login page.
		locals.auth.setSession(session);
        } catch (e) {
            console.error(e);
            if (e instanceof LuciaError ) {
                throw error(400, e.message);
            }
            throw error(500, 'Internal Server Error');
        }
		// Redirect the user to the home page.
        throw redirect(303, '/');
	}
}
