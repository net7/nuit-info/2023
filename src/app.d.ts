// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
/// <reference types="lucia" />
declare global {
	/// <reference types="lucia-auth" />
	declare namespace Lucia {
		type Auth = import('$lib/server/lucia').Auth;
		type DatabaseUserAttributes = {
			name: string;
			email: string;
			admin: boolean;
			isAsso: boolean;
			bio: string;
			avatar: string;
			website: string;
		};
		type DatabaseSessionAttributes = {};
	}
	
	namespace App {
		interface Locals {
			auth: import("lucia").AuthRequest;
		}
	}

	/// <reference types="@sveltejs/kit" />
	declare namespace App {
		interface Locals {
			validate: import('@lucia-auth/sveltekit').Validate;
			validateUser: import('@lucia-auth/sveltekit').ValidateUser;
			setSession: import('@lucia-auth/sveltekit').SetSession;
		}
	}

	var prisma: import('@prisma/client').PrismaClient;
}

export {};
