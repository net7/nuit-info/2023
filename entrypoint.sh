#!/bin/sh
set -xe

# Generate prisma client
yarn prisma migrate deploy

# Start the app
node build/index.js