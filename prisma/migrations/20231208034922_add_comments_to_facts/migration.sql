-- AlterTable
ALTER TABLE "Comment" ADD COLUMN     "fact_id" TEXT;

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_fact_id_fkey" FOREIGN KEY ("fact_id") REFERENCES "Fact"("id") ON DELETE SET NULL ON UPDATE CASCADE;
