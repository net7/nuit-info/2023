/*
  Warnings:

  - Added the required column `location` to the `Demonstration` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Demonstration" ADD COLUMN     "location" TEXT NOT NULL;
