import { PrismaClient } from '@prisma/client';
import { auth } from '../src/lib/server/lucia';
import { readFileSync } from 'fs';

async function setBase64Picture(email: string) {
	const imgContents = readFileSync(`./Images/${email}.base64`, { encoding: 'utf-8' });
	await prisma.user.update({
		where: {
			email
		},
		data: {
			avatar: imgContents
		}
	});
}

const prisma = new PrismaClient();

await prisma.tag.createMany({
	data: ['Impact', 'Future', 'Idées reçues', 'Comprendre', 'Responsabilité'].map((name) => ({
		name,
		description: name,
		color: '#ffffff'
	}))
});

await auth.createUser({
	attributes: {
		name: 'Admin',
		email: 'admin@net7.dev',
		admin: true
	},
	key: {
		providerId: 'local',
		providerUserId: 'admin@net7.dev',
		password: 'net7'
	}
});

await auth.createUser({
	attributes: {
		name: 'La CGT',
		email: 'contact@cgt.fr',
		isAsso: true
	},
	key: {
		providerId: 'local',
		providerUserId: 'contact@cgt.fr',
		password: 'quoicoubeh'
	}
});

setBase64Picture('contact@cgt.fr');

await auth.createUser({
	attributes: {
		name: 'Greenpeace',
		email: 'contact@greenpeace.fr',
		isAsso: true
	},
	key: {
		providerId: 'local',
		providerUserId: 'contact@greenpeace.fr',
		password: 'apagnan'
	}
});

setBase64Picture('contact@greenpeace.fr');

await auth.createUser({
	attributes: {
		name: 'L214',
		email: 'contact@l214.com',
		isAsso: true
	},
	key: {
		providerId: 'local',
		providerUserId: 'contact@l214.com',
		password: 'chickenpolice'
	}
});

setBase64Picture('contact@l214.com');

await auth.createUser({
	attributes: {
		name: 'Annie Versaire',
		email: 'annieversaire@outlook.com',
		isAsso: false
	},
	key: {
		providerId: 'local',
		providerUserId: 'annieversaire@outlook.com',
		password: 'a'
	}
});

setBase64Picture('annieversaire@outlook.com');

await auth.createUser({
	attributes: {
		name: 'Téo Pisenti',
		email: 'superteo@gmail.com',
		isAsso: false
	},
	key: {
		providerId: 'local',
		providerUserId: 'superteo@gmail.com',
		password: 'minecraftisgreat'
	}
});

setBase64Picture('superteo@gmail.com');

await auth.createUser({
	attributes: {
		name: 'Marche pour le climat',
		email: 'contact@marcheclimat.fr',
		isAsso: true
	},
	key: {
		providerId: 'local',
		providerUserId: 'contact@marcheclimat.fr',
		password: 'marchepourleclimat'
	}
});

setBase64Picture('contact@marcheclimat.fr');

await auth.createUser({
	attributes: {
		name: 'Toulouse Manif',
		email: 'contact@toulousemanif.fr',
		isAsso: true
	},
	key: {
		providerId: 'local',
		providerUserId: 'contact@toulousemanif.fr',
		password: 'marchepourleclimat'
	}
});

setBase64Picture('contact@toulousemanif.fr');

await auth.createUser({
	attributes: {
		name: 'EELV',
		email: 'contact@eelv.fr',
		isAsso: true
	},
	key: {
		providerId: 'local',
		providerUserId: 'contact@eelv.fr',
		password: 'eeeeeeelv'
	}
});

setBase64Picture('contact@eelv.fr');

await prisma.demonstration.create({
	data: {
		description:
			'Manifestation contre la décision de la mairie de Toulouse cherchant à construire un carrefour à la place du parc',
		endsAt: new Date('2024-09-22T10:00:00.000Z'),
		startsAt: new Date('2024-08-22T18:00:00.000Z'),
		name: 'Contre la destruction du parc de Toulouse Borderouge',
		location: 'Toulouse',
		path: '',
		creator: {
			connect: {
				email: 'contact@toulousemanif.fr'
			}
		}
	}
});

await prisma.demonstration.create({
	data: {
		description: 'Marche pour le Climat en réponse aux inaction du gouvernement',
		endsAt: new Date('2024-09-01T12:00:00.000Z'),
		startsAt: new Date('2024-09-01T18:00:00.000Z'),
		name: 'Marche pour le Climat',
		path: '',
		location: 'Paris',
		creator: {
			connect: {
				email: 'contact@marcheclimat.fr'
			}
		}
	}
});

await prisma.demonstration.create({
	data: {
		description:
			'Manifestation contre le projet de mega bassine annoncé par le gouvernement dans la région du ',
		endsAt: new Date('2023-12-18T15:00:00.000Z'),
		startsAt: new Date('2023-12-18T18:00:00.000Z'),
		name: 'Manifestation contre le nouveau projet de mega bassine',
		path: '',
		location: 'La rochelle',
		creator: {
			connect: {
				email: 'contact@greenpeace.fr'
			}
		},
		participants: {
			connect: ['contact@l214.com', 'contact@toulousemanif.fr'].map((email) => ({ email }))
		}
	}
});

await prisma.petition.create({
	data: {
		description: 'Interdire la circulation des véhicules critair 4',
		endsAt: new Date('2023-09-12T00:00:00.000Z'),
		name: 'Petition lancé par Europe écologie les verts afin de faire interdire la circulation des véhicules poluants en France',
		startsAt: new Date('2023-08-12T00:00:00.000Z'),
		signaturesGoal: 40,
		creator: { connect: { email: 'contact@eelv.fr' } }
	}
});

const petition = await prisma.petition.create({
	data: {
		description: 'Forçons les GAFAM à revoir leur politique écologique sur le sol Européen',
		endsAt: new Date('2023-09-20T00:00:00.000Z'),
		name: `Petition soutenu par Greenpeace afin d'obliger les 
		géant du numérique à être en accord avec la législation Européenne 
		circulation des véhicules poluants en France`,
		startsAt: new Date('2023-08-20T00:00:00.000Z'),
		creator: { connect: { email: 'contact@greenpeace.fr' } },
		signaturesGoal: 1000,
		participants: { connect: ['contact@eelv.fr', 'superteo@gmail.com'].map((email) => ({ email })) }
	}
});

await prisma.petition.create({
	data: {
		description: `Libérer l'activiste Jean Naimare`,
		endsAt: new Date('2023-09-24T00:00:00.000Z'),
		name: `Petition créer par la famille de l'activite écologiste Jean Naimare qui  
		dénonce les exactions écologiste du gouvernement`,
		startsAt: new Date('2023-08-20T00:00:00.000Z'),
		signaturesGoal: 500,
		creator: { connect: { email: 'contact@greenpeace.fr' } }
	}
});

await prisma.fact.create({
	data: {
		content: `Il est possible de déterminer de façon quantitative comment l’influence humaine 
		a joué sur  une canicule en étudiant sa probabilité d’occurrence ainsi que son intensité, 
		comme expliqué dans cet article du Monde. Par exemple, ces calculs permettent d’estimer que
		les chances que la canicule de juillet 2019 en France se produise étaient de 1 sur 40. 
		Sans réchauffement climatique, cette probabilité n’aurait été que de 1 sur 20 000. En termes statistique, on peut 
		affirmer que cette canicule était virtuellement impossible sans influence humaine. 
		Par ailleurs, le rapport du GIEC montre que l’influence humaine rend les vagues 
		de chaleur plus fréquentes, plus intenses et plus précoces. On peut constater sur
		le graphique suivant que ces extrêmes de chaleur sont en augmentation dans le 
		monde entier. Le GIEC rapporte enfin que la probabilité d’occurrence d’événements inédits
		et très impactants augmentent avec chaque dixième de réchauffement global.`,
		name: `Réchauffement climatique : des vagues de chaleur plus fréquentes et plus intenses`
	}
});

await prisma.fact.create({
	data: {
		content: `La voiture électrique est en effet plus émettrice de gaz à effet
		 de serre sur la phase de production du véhicule. Ce surplus d’émissions 
		 est d’environ +50 % (mais varie d’environ +20 % à plus d’une multiplication par 2 selon les études) 
		 et est essentiellement dû à la fabrication de la batterie. En revanche, les émissions 
		 à l’usage de la voiture électrique sont facilement 15 fois plus faibles que pour 
		 le thermique  ien France. Non seulement parce que les consommations d’énergie 
		 finales sont environ 3 fois plus faibles pour l’électrique, grâce à un moteur 
		 plus efficace (ce qui limiterait le surplus de consommation d’électricité 
		 nécessaire aux voitures électriques à 20 % des consommations actuelles, même 
		 sans sobriété). Mais aussi parce que l’électricité en France émet environ 5 à 6 
		 fois moins de CO2 par unité d’énergie que de produire et brûler du carburant 
		 pétrolier.  <a href="https://bonpote.com/la-voiture-electrique-solution-ideale-pour-le-climat/">En savoir plus</a>`,
		name: `Est-ce que la fabrication de voiture électrique est autant polluant que l'on peut l'entendre ?`
	}
});

const fact = await prisma.fact.create({
	data: {
		content: `Des solutions concrètes, efficaces et accessibles existent 
		pour les limiter. Tout d’abord, il est indispensable de réduire rapidement 
		et fortement nos émissions de gaz à effet de serre, responsables du changement
		climatique. Pour cela, de nombreuses mesures existent, à commencer par 
		arrêter l’exploitation des énergies fossiles (charbon, pétrole, gaz), tendre 
		vers une société plus sobre (en énergie, matériaux…) ou encore investir dans les 
		énergies renouvelables : le GIEC détaille les options existantes dans son rapport
		 sur l’atténuation du changement climatique.En parallèle, il est nécessaire 
		 de déployer des mesures afin de nous adapter à ces conséquences déjà présentes et 
		 anticiper celles à venir. Par exemple, en repensant et en végétalisant les villes 
		 pour y limiter l’impact de la chaleur, ou en développant une agriculture plus 
		 résiliente au changement climatique, comme l’agriculture biologique, plus 
		 économe en eau et meilleure pour les sols. <a href="https://reseauactionclimat.org/quels-sont-les-impacts-du-changement-climatique-en-france/">En savoir plus</a>`,
		name: `Des solutions pour lutter contre l'emission des gaz à effet de serre ?`
	}
});

await prisma.comment.create({
	data: {
		text: "C'est honteux, merci de partager ça !",
		user: { connect: { email: 'superteo@gmail.com' } }
	}
});

await prisma.comment.create({
	data: {
		text: 'Merci pour cet article de qualité, cependant cela me semble difficile à mettre en place !',
		user: { connect: { email: 'superteo@gmail.com' } }
	}
});

await prisma.comment.create({
	data: {
		text: 'Je suis deter pour changer mes habitudes !',
		user: { connect: { email: 'annieversaire@outlook.com' } },
		petition: { connect: { id: petition.id } }
	}
});

await prisma.comment.create({
	data: {
		text: "Oubliez pas de d'aller voir nos autres actions sur notre site web !",
		user: { connect: { email: 'contact@l214.com' } }
	}
});
